package model;

        import java.io.File;
        import java.io.FileInputStream;
        import java.io.FileNotFoundException;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.util.Properties;

        import view.Output;

public class Config {

    Properties config;

    public Config(){
        config = new Properties();
        try {
            FileInputStream in = new FileInputStream(new File("nowayrc-client.conf"));
            config.load(in);
        } catch (FileNotFoundException e) {
            try {
                save("localhost", "1248", "DefaultName");
            } catch (Exception e1) {
                Output.log("CONF", "Error saving default config");
            }
        } catch (IOException e) {
            Output.log("CONF", "Error reading config file");
        }
    }

    public String getHost() {
        return config.getProperty("host");
    }
    public String getPort() {
        return config.getProperty("port");
    }
    public String getName(){
        return config.getProperty("name");
    }

    public void save(String s, String p, String n){
        try{
            config.put("host", s);
            config.put("port", p);
            config.put("name", n);

            config.store(new FileOutputStream(new File("nowayrc-client.conf")), "Default config for nowayrc-client");
        } catch (Exception e1) {
            Output.log("CONF", "Error saving default config");
        }
    }
}


		
