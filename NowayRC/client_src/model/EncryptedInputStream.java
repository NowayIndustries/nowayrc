package model;



import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class EncryptedInputStream extends EncryptedStream {

	private ObjectInputStream in;
	
	public EncryptedInputStream(InputStream in, String secret)
	{
		this.privateSecret = secret;
		
		try {
			this.in = new ObjectInputStream(in);
		} catch (IOException e) {
			System.out.println("EOUTS=Error when creating socket.");
			e.printStackTrace();
		}
	}
	
	public String receive()
	{
		String msg = "Error";
		try {
			msg = (String)in.readObject();
			msg = decrypt(msg);
		} catch (IOException e) {
			System.out.println("EINS=Error when reading from socket.");
		} catch (ClassNotFoundException e) {
			System.out.println("EINS=User has send an unknown datatype!");
		}
		return msg;
	}
}
