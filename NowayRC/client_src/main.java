import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import view.ClientPanel;
import controller.Luisteraar;
import model.EncryptedInputStream;
import model.EncryptedOutputStream;


public class main {
	
	public static void main(String[] args)
	{
			JFrame f = new JFrame("NowayRC Client - Disconnected");
            ClientPanel cp = new ClientPanel(f);
			f.add(cp);
			f.setSize(1000 , 800);
			f.pack();
			f.setLocationRelativeTo(null);
            f.addKeyListener(cp);
			f.setVisible(true);
	}
}
