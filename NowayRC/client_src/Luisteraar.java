import java.io.IOException;
import java.io.ObjectInputStream;


public class Luisteraar implements Runnable{

	ObjectInputStream in;
	boolean running = true;
	
	public Luisteraar(ObjectInputStream in)
	{
		this.in = in;
	}

	@Override
	public void run() {
		while(running)
		{
			try {
				
				String received = (String)in.readObject();
				if(received.substring(0, 2).compareToIgnoreCase("0x") != 0)
				{
					System.out.println(received);
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
		}
		
	}

	public void stop() {
		running = false;
		
	}
}