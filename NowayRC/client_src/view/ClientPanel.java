package view;

import controller.Luisteraar;
import model.Config;
import model.EncryptedInputStream;
import model.EncryptedOutputStream;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;

public class ClientPanel extends JPanel implements ActionListener, KeyListener{

    JFrame container;
	JPanel connectionPanel; //Settings for server
	JPanel connectedPanel; //Shows connected users
	JPanel textPanel; //Contains the chat messages
	JPanel inputPanel; //has input field and send button (maybe enter listener?)
	
	//connectionPanel
	JTextField textServer;
	JTextField textPort;
	JTextField textPassword;
	JTextField textName;
	
	JButton buttonConnect;
    JButton buttonExit;
	
	//connectedPanel
	JList listConnected;
    DefaultListModel model;
	
	//textPanel
	JTextArea textChat;
	
	//inputPanel
	JTextField textInput;
	
	JButton buttonSend;

    //chat
    Thread t;
    Luisteraar listener;
    ObjectOutputStream output;
    EncryptedInputStream input;
    Config config;
	
	public ClientPanel(JFrame container)
	{
        config = new Config();
        this.container = container;
		createConnectionPanel();
		createConnectedPanel();
        createInputPanel();
		createTextPanel();

        this.setLayout(new BorderLayout());

        JPanel left = new JPanel();
        left.setLayout(new BorderLayout());
		left.add(connectionPanel, BorderLayout.NORTH);
        left.add(connectedPanel, BorderLayout.CENTER);

        this.add(left, BorderLayout.WEST);
        this.add(textPanel, BorderLayout.EAST);

	}
	
	public void createConnectionPanel()
	{
		connectionPanel = new JPanel();
		connectionPanel.setBorder(new TitledBorder("Server information:"));
		connectionPanel.setLayout(new GridLayout(0, 2));
		
		connectionPanel.add(new JLabel("Server:"));
		textServer = new JTextField(config.getHost());
        textServer.addKeyListener(this);
		connectionPanel.add(textServer);
		
		connectionPanel.add(new JLabel("Port:"));
		textPort = new JTextField(config.getPort());
        textPort.addKeyListener(this);
		connectionPanel.add(textPort);
		
		connectionPanel.add(new JLabel("Pass:"));
		textPassword = new JPasswordField();
        textPassword.addKeyListener(this);
		connectionPanel.add(textPassword);
		
		connectionPanel.add(new JLabel("Name:"));
		textName = new JTextField(config.getName());
        textName.addKeyListener(this);
		connectionPanel.add(textName);

        buttonExit = new JButton("Exit");
        buttonExit.addActionListener(this);
		connectionPanel.add(buttonExit);
		buttonConnect = new JButton("Connect!");
		buttonConnect.addActionListener(this);
		connectionPanel.add(buttonConnect);
	}

    public void createConnectedPanel()
    {
        connectedPanel = new JPanel();
        connectedPanel.setBorder(new TitledBorder("Connected users:"));
        connectedPanel.setPreferredSize(new Dimension(150,200));
        connectedPanel.setLayout(new BorderLayout());

        model = new DefaultListModel();
        listConnected = new JList(model);

        JScrollPane scroll = new JScrollPane(listConnected);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        connectedPanel.add(scroll, BorderLayout.CENTER);
    }

    public void createTextPanel()
    {
        textPanel = new JPanel();
        textPanel.setBorder(new TitledBorder("Chat:"));
        textPanel.setPreferredSize(new Dimension(700, 500));
        textPanel.setLayout(new BorderLayout());

        textChat = new JTextArea();
        textChat.setEditable(false);
        textChat.setLineWrap(true);
        DefaultCaret caret = (DefaultCaret)textChat.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);


        JScrollPane scroll2 = new JScrollPane(textChat);
        scroll2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        textPanel.add(scroll2, BorderLayout.CENTER);
        textPanel.add(inputPanel, BorderLayout.SOUTH);
    }

    public void createInputPanel()
    {
        inputPanel = new JPanel();
        inputPanel.setLayout(new BorderLayout());

        textInput = new JTextField();
        textInput.addKeyListener(this);

        buttonSend = new JButton("Send");
        buttonSend.addActionListener(this);
        buttonSend.setEnabled(false);

        inputPanel.add(textInput, BorderLayout.CENTER);
        inputPanel.add(buttonSend, BorderLayout.EAST);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == buttonConnect){
            connect();

        }else if(e.getSource() == buttonSend){
            sendMsg();
        }else if(e.getSource() == buttonExit){
            disconnect();
            config.save(textServer.getText(), textPort.getText(), textName.getText());
            System.out.println("Exiting...");
            System.exit(0);
        }

	}
    public void keyTyped(KeyEvent e) { }
    public void keyPressed(KeyEvent e) { }
    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_ENTER)
        {
            if(output == null){
                connect();
            }else{
                sendMsg();
            }
        }
    }

    private void sendMsg()
    {
        try{output.writeObject(textInput.getText());}catch(Exception e17){}finally {
        textInput.setText("");
        }
    }

    private void disconnect(){
        try{output.writeObject("0x000002");listener.stop();}catch(Exception e5){}
    }

    private void connect()
    {
        try{
            if(output != null)
                disconnect();

            String secret = textPassword.getText();

            Socket socket = new Socket(textServer.getText(), Integer.parseInt(textPort.getText()));
            output = new ObjectOutputStream(socket.getOutputStream());
            output.writeObject("1;" + textPassword.getText() + ";" + textName.getText() + ";" + textPassword.getText());

            input =  new EncryptedInputStream(socket.getInputStream(), secret);

            listener = new Luisteraar(input, textChat, model);
            t = new Thread(listener);
            t.start();

            buttonSend.setEnabled(true);
            container.setTitle("NowayRC Client - "+textName.getText()+"@"+textServer.getText());

            output.writeObject("0x000003");
            textInput.requestFocusInWindow();

        } catch(NumberFormatException e3){
            JOptionPane.showMessageDialog(container, "Please enter a valid port number!");
        } catch(ConnectException e4){
            JOptionPane.showMessageDialog(container, "Invalid server information entered!");
        }catch(Exception e2){
            JOptionPane.showMessageDialog(container, "Something went wrong!");
            e2.printStackTrace();
        }
    }
}
