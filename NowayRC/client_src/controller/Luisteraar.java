package controller;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import model.EncryptedInputStream;
import view.Output;

import javax.swing.*;


public class Luisteraar implements Runnable{

	EncryptedInputStream in;
    JTextArea chat;
    DefaultListModel model;
	boolean running = true;
	
	public Luisteraar(EncryptedInputStream in, JTextArea chat, DefaultListModel m)
	{
		this.in = in;
        this.chat = chat;
        this.model = m;
	}

	@Override
	public void run() {
		while(running)
		{
			try {
				
				String received = (String)in.receive();
                String sub = (received.substring(0, 15));
                if(sub.compareToIgnoreCase("[PLAYERLISTING]") == 0)
                {
                    Output.log("Playerlisting received from server");
                    updatePlayerList(received);
                }else if(received.substring(0, 2).compareToIgnoreCase("0x") != 0)
				{
					chat.setText(chat.getText() +"\n"+received);
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
		}
	}

    public void updatePlayerList(String r)
    {
        String[] split = r.split(" ", 2);
        String players = split[1];
        players = players.replace("[ ", "");
        players = players.replace(" ]", "");

        split = players.split(" | ");
        ArrayList<String> playersList = new ArrayList<String>(Arrays.asList(split));
        while(playersList.contains("|"))
        {
            playersList.remove("|");
        }

        model.clear();
        for(String s : playersList){
            model.addElement(s);
        }
    }

	public void stop() {
		running = false;
		
	}
}