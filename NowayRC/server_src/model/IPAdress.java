package model;

import java.net.*;
import java.io.*;

import view.Output;

public class IPAdress {
	
	/**
	 * Resolves the external ip by connection to a webservice that gives the ip in raw text.
	 * @return The external ip the server has when connecting to other servers.
	 */
	public static String getExternalIP()
	{
		Output.log("IP", "Resolving ipadress... (this might take a while)");
		boolean resolved = false;
		String server = "";
		for(int i = 0; i < 1 && !resolved; i++)
		{
			switch(i)
			{
			case 0:
				server = "http://checkip.amazonaws.com/";
				break;
			/*//Template for expanding with extra sites, don't forget to change the loop counter.
			case 1:
			 	server = "website that gives ip in raw text";
			 	break;
			
			 */
			}
			try{
				URL ipresolver = new URL(server);
				BufferedReader in = new BufferedReader(new InputStreamReader(ipresolver.openStream()));
			
				return in.readLine();
			}
			catch(MalformedURLException MURLE)
			{
				Output.log("IP", "Unable to connect to first IP resolving website, " +
							"please note the following error code: 'IP-NO.PROTOCOL/URL.PARSE.ERROR." + i +"' when submitting a bug report.");
			}
			catch(IOException IOE)
			{
				Output.log("IP", "Unable to connect to first IP resolving website (are you connected to the internet?), " +
							"please note the following error code: 'IP-IO.FAILED." + i +"' when submitting a bug report.");
			}
		}
		return "localhost";
	}
	
}
