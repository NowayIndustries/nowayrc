package model;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import view.Output;

public class EncryptedOutputStream extends EncryptedStream {
	
	private ObjectOutputStream out;
	
	public EncryptedOutputStream(ObjectOutputStream out, String secret)
	{
		this.privateSecret = secret;
		this.out = out;
	}
	
	public void send(String prefix, String msg)
	{
		msg = decrypt(msg);
		msg = prefix+msg;
		try {
			out.writeObject(encrypt(msg));
		} catch (IOException e) {
			Output.log("EOUTS", "Error when writing to socket.");
		}
	}

    public void sendRaw(String msg)
    {
        try {
            out.writeObject(encrypt(msg));
        } catch (IOException e) {
            Output.log("EOUTS", "Error when writing raw to socket.");
        }
    }
}
