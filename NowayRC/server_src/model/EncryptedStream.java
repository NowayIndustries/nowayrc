package model;

public abstract class EncryptedStream {
	
	String privateSecret;
	String sharedSecret;
	String protocolVersion;
	
	protected String encrypt(String msg)
	{
		//do something clever using the private and shared secret to obfuscate communications
		
		return msg;
	}
	
	protected String decrypt(String msg)
	{
		//reverse the clever thing done by the encrypter
		return msg;
	}
	
}
