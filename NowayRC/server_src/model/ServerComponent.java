package model;

import controller.ClientHandler;
import controller.CommandHandler;
import controller.ConnectionHandler;

public interface ServerComponent {
	
	public Config config = new Config();
	
	public ConnectionHandler connectionHandler = new ConnectionHandler();
	public CommandHandler commandHandler = new CommandHandler();
	public ClientHandler clientHandler = new ClientHandler();
	
	public Thread connectionHandlerThread = new Thread( connectionHandler );
	public Thread commandHandlerThread = new Thread( commandHandler );
	public Thread clientHandlerThread = new Thread( clientHandler );
	
	/**
	 * Called at the start of the component, for example it is the first call in the run() method.
	 * (only applicable if the component is runnable).
	 */
	public void onStart();
	/**
	 * Called when component has to be shutdown.
	 */
	public void onShutdown();
	/**
	 * Called when the component has to stop, for example it breaks the while loop in runnable components.
	 */
	public void shutdown();

}
