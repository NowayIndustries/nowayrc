package model;

import java.io.File;

import javax.swing.JOptionPane;

import view.Output;

public abstract class Commands implements ServerComponent {
	public static String[] commandList = { "SAY", "KICK", "BAN", "STOP", "UNBAN", "HELP", "LIST", "CHECK", "VERSION", "RESET" };
	public static String[] textCommandList = { "IP", "LIST", "MOTD", "HELP", "VERSION" };
	//must always contain atleast one command.
	
	/**
	 * Sends the parameter to all clients with sender name 'server'.
	 * @param msg Message to be sent.
	 */
	public static void say(String msg)
	{
		clientHandler.sendAll("Server", msg);
	}
	/**
	 * Kicks the user specified in the parameter.
	 * @param msg Contains the username and if specified reason for kicking.
	 */
	public static void kick(String msg)
	{
		String[] processedString = extractCommand(msg);
		String user, reason;
		
		if(processedString.length == 1)
		{
			user = processedString[0];
			reason = "You have been kicked from this server.";
			clientHandler.disconnect(user, reason);
		}
		else if (processedString.length == 2)
		{
			user = processedString[0];
			reason = processedString[1];
			clientHandler.disconnect(user, reason);
		}
		else
		{
			Output.log("Error: no user specified, try 'kick username reason'");
		}
	}
	/**
	 * Bans the player with the specified name.
	 * @param input Contains the name and if specified the reason.
	 */
	public static void ban(String input)
	{
		String[] processedString = extractCommand(input);
		String user = "hacker";
		String reason = "hacker";
		
		if(processedString[0].compareTo("") == 0)
		{
			connectionHandler.printBlacklist();
			return;
		}
		
		if(processedString.length == 1)
		{
			user = processedString[0];
			reason = "a valid reason";
			
		}
		else if (processedString.length == 2)
		{
			user = processedString[0];
			reason = processedString[1];
		}
		if(user.compareToIgnoreCase("hacker") != 0 && reason.compareToIgnoreCase("hacker") != 0)
		{
			if(connectionHandler.addToBlacklist(user))
			{
				Output.log("User '" +user+ "' has been banned for '" +reason+ "'.");
			}
			clientHandler.disconnect(user, reason);
			
		}
		else
		{
			Output.log("Error: no user specified, try 'ban username reason'");
		}
	}
	
	/**
	 * Splits the string on a space, thus returning the command and the parameter.
	 * @param input String to be split.
	 * @return The String array that contains the 2 elements of input.
	 */
	private static String[] extractCommand(String input)
	{
		return input.split(" ", 2);
	}
	/**
	 * Stops the server, by telling the exit method in main to run.
	 */
	public static void stop() {
		Output.log("Stopping server.");
		main.main.exit();
	}
	/**
	 * Unbans the specified player.
	 * @param parameter Username to be removed from blacklist.
	 */
	public static void unban(String parameter) {
		String[] processedString = extractCommand(parameter);
		String user = processedString[0];
		connectionHandler.removeFromBlacklist(user);
		Output.log("Unbanning '" +user+ "'.");
	}
	/**
	 * Prints the available commands for the server application.
	 */
	public static void help(){
		String commands = "["+commandList[0];
		for(int i = 1; i < Commands.commandList.length; i++)
		{
			commands += " | ";
			commands += Commands.commandList[i];
		}
		commands += "]";
		Output.log("Accepted commands: " +commands +"\nUse 'ban' without parameters to see banned users.");
	}
	/**
	 * Lists the connected clients to the server application.
	 */
	public static void list(){
		Output.log("LIST", clientHandler.getList());
	}
	/**
	 * Checks the connected sockets, to find dead connections.
	 * Prints the amount of dead and alive connections to server application.
	 */
	public static void check(){
		Output.log("CHECK", clientHandler.check());
	}
	/**
	 * Prints the server version.
	 */
	public static void version(){
		Output.log("INFO", "Server software version: '"+config.getVersion()+ "'.");
	}
	public static void reset(String parameter){
		String[] processedString = extractCommand(parameter);
		if(processedString.length == 0 || parameter.compareTo("") == 0){
			Output.log("RESET", "You can either reset the: 'BANS', 'CONFIG' or 'CHATLOGS' files.");
			return;
		}
		String filename = "ERROR";
		if(processedString[0].compareToIgnoreCase("BANS") == 0)
		{
			filename = config.getBlackListFile();
		}
		if(processedString[0].compareToIgnoreCase("CONFIG") == 0)
		{
			filename = "nowayrc.conf";
		}
		if(processedString[0].compareToIgnoreCase("CHATLOGS") == 0)
		{
			filename = "chatlogs";
		}
		File file = new File(filename);
		if(!file.isDirectory())
		{
			if(file.delete()){
				Output.log("RESET", "Deleted '"+filename+"', blacklist will be rewritten on server stop, config on start, chatlogs on start.");
			}else{
				Output.log("RESET", "Unable to delete '"+filename+"', will schedule for delete on exit. (You could always manually delete the file '"+config.getBlackListFile()+"')");
				file.deleteOnExit();
			}
		}else{
			for(File f : file.listFiles())
			{
				if(f.delete()){
					Output.log("RESET", "Deleted '"+f.getName()+"', blacklist will be rewritten on server stop, config on start, chatlogs on start.");
				}else{
					Output.log("RESET", "Unable to delete '"+f.getName()+"', will schedule for delete on exit. (You could always manually delete the file '"+config.getBlackListFile()+"')");
					f.deleteOnExit();
				}
			}
		}
	}
	
	
		
	/**
	 * Broadcasts the server application's external ip.
	 */
	public static String[] textIp(){
        String[] d = {"IP", "The server's external ip is '" +IPAdress.getExternalIP()+ "'." };
		return d;
	}
	/**
	 * Broadcasts the list of connected clients.
	 */
	public static String[] textList(){
		String[] d ={"LIST", clientHandler.getList() };
        return d;
	}
	/**
	 * Broadcasts the Message Of The Day (MOTD).
	 */
	public static String[] textMotd(){
        String[] d = {"MOTD", config.getMotd() };
        return d;
	}
	/**
	 * Broadcasts the availble commands.
	 */
	public static String[] textHelp(){
		String commands = "["+textCommandList[0];
		for(int i = 1; i < Commands.textCommandList.length; i++)
		{
			commands += " | ";
			commands += Commands.textCommandList[i];
		}
		commands += "]";
        String[] d = {"HELP","Accepted commands: " +commands };
        return d;
	}
	/**
	 * Broadcasts the servers software version
	 */
	public static String[] textVersion(){
        String[] d = {"INFO", "Server software version: '" +config.getVersion()+ "'." };
        return d;
	}
}
