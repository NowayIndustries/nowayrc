package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import view.Output;

public class Client
{
	private int UUID;
	private String name;
	private Socket socket;
	private EncryptedOutputStream out;
	private EncryptedInputStream in;
	
	/**
	 * This constructor should only be used for the initial connection.
	 * @param s
	 * @param in
	 * @param out
	 * @param n
	 */
	public Client(Socket s, EncryptedInputStream in, EncryptedOutputStream out, String n)
	{
		this(s, in, out, n, null, null, null);
	}
	/**
	 * Constructor to create a new client.
	 * @param s Socket for the client.
	 * @param in ObjectInputStream for the client.
	 * @param out ObjectOutputStream for the client.
	 * @param n The client's name.
	 * @param protocol Communication protocol version to use.
	 * @param sharedsecret The sharedsecret used to obfuscate the communication.
	 */
	public Client(Socket s, EncryptedInputStream in, EncryptedOutputStream out, String n, String secret, String sharedSecret, String protocol)
	{
		socket = s;
		this.in = new EncryptedInputStream (in.getInputStream(), secret, sharedSecret, protocol);
		this.out = out;
		this.name = n;
		
	}
	
	
	/**
	 * Closes the socket.
	 */
	public void disconnect()
	{
		try {
			socket.close();
		} catch (IOException e) {
		}
	}
	
	/**
	 * Sends the specified string to the user by writing it to the socket.
	 * @param prefix The prefix for the message
	 * @param msg The string to send.
	 */
	public void send(String prefix, String msg)
	{
		this.getEncryptedOutputStream().send(prefix, msg);
	}
	
	/**
	 * Returns current status of client, true for good, false for disconnected/whatever.
	 * @return status of client.
	 */
	public boolean getStatus()
	{
		return true;
	}
	public int getUUID()
	{
		return UUID;
	}
	public String getName()
	{
		return name;
	}
	public Socket getSocket()
	{
		return socket;
	}
	public EncryptedOutputStream getEncryptedOutputStream()
	{
		return out;
	}
	public EncryptedInputStream getEncryptedInputStream()
	{
		return in;
	}

	private void setName(String n) 
	{
		this.name = n;
	}
}
