package model;

public class Protocol {

	public static String getCheckCode()
	{
		return "0x000001";
	}
	
	public static String getDisconnectCode()
	{
		return "0x000002";
	}

    public static String getListCode()
    {
        return "0x000003";
    }
	
	public static String getSeperator()
	{
		return ";";
	}
	
	public static String getCommandCode()
	{
		return "@";
	}
}
