package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import view.Output;

public class Config {

    Properties config;
    String[] supportedVersions = { "1" };
    String versionString = "2.0 PUBLIC RELEASE";

    public Config(){
        config = new Properties();
        try {
            FileInputStream in = new FileInputStream(new File("nowayrc.conf"));
            config.load(in);
        } catch (FileNotFoundException e) {
            try {
                config.put("port", "1248");
                config.put("host", "localhost");
                config.put("motd", "Welcome to this server!");
                config.put("maxConnectedClients", "10");
                config.put("blacklistFile", "nowayrc.blacklist");
                config.put("authserver", "");
                config.put("serverpass", "");
                config.store(new FileOutputStream(new File("nowayrc.conf")), "Default config for nowayrc");
            } catch (Exception e1) {
                Output.log("CONF", "Error saving default config");
            }
        } catch (IOException e) {
            Output.log("CONF", "Error reading config file");
        }
    }
    public String getMotd(){
        return config.getProperty("motd");
    }

    public String getVersion() {
        return versionString;
    }
    public int getMaxConnections() {
        return Integer.parseInt(config.getProperty("maxConnectedClients"));
    }
    public String getHost() {
        return config.getProperty("host");
    }
    public int getPort() {
        return Integer.parseInt(config.getProperty("port"));
    }
    public String getBlackListFile(){
        return config.getProperty("blacklistFile");
    }

    public String[] getSupportedProtocolVersions()
    {
        return supportedVersions;
    }

    public String getAuthServerUrl()
    {
        return config.getProperty("authserver");
    }
    public String getServerPassword()
    {
        return config.getProperty("serverpass");
    }
}