package model;


import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

import view.Output;

public class EncryptedInputStream extends EncryptedStream {

	private ObjectInputStream in;
	
	public EncryptedInputStream(ObjectInputStream in, String secret)
	{
		this(in, secret, null, null);
	}
	
	public EncryptedInputStream(ObjectInputStream in, String secret, String sharedSecret, String version)
	{
		this.privateSecret = secret;
		this.in = in;
		this.sharedSecret = sharedSecret;
		this.protocolVersion = version;
	}
	
	public String receive()
	{
		String msg = "Error";
		try {
			msg = (String)in.readObject();
			msg = decrypt(msg);
			return msg;
		} catch (IOException e) {
			Output.log("EINS", "Error when reading from socket, closing socket.");
			try {
				in.close();
			} catch (IOException e1) {
				Output.log("EINS", "Error when closing socket!");
			}
		} catch (ClassNotFoundException e) {
			Output.log("EINS", "User has send an unknown datatype!");
		}
		return null;
	}
	protected ObjectInputStream getInputStream()
	{
		return this.in;
	}
	public void close()
	{
		try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Output.log("EINS", "Unable to close socket, expect increased RAM usage.");
		}
	}
}
