package view;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class Output {
	
	/**
	 * Provides you with a fulle timestamp.
	 * @return The full timestamp for example: '[Wed Sep 18 10:26:04 CEST 2013]'.
	 */
	private static String getFullTimeStamp()
	{
		Calendar calendar = new GregorianCalendar();
		return "["+ calendar.getTime().toString() + "] ";	
	}
	/**
	 * Returns a time only timestamp.
	 * @return The timestamp, for example '[10:26:04]'.
	 */
	public static String getTimeStamp()
	{
		Calendar calendar = new GregorianCalendar();
		String hour = ""+calendar.get(Calendar.HOUR_OF_DAY);
		String minute = ""+calendar.get(Calendar.MINUTE);
		String second = ""+calendar.get(Calendar.SECOND);
		
		if(hour.length() == 1)
			hour = "0"+hour;
		if(minute.length() == 1)
			minute = "0"+minute;
		if(second.length() == 1)
			second = "0"+second;
		
		return "["+ hour + ":" + minute + ":" + second + "] ";	
	}
	
	public static void log(String msg)
	{
		System.out.println(getTimeStamp() + msg);
	}
	/**
	 * Logs with the specified prefix. ([TIMESTAMP] [PREFIX] MSG)
	 * @param prefix
	 * @param msg
	 */
	public static void log(String prefix, String msg)
	{
		log("[" +prefix+ "] " +msg);
	}
	/**
	 * Start message which outputs a full timestamp with the adress the server is being hosted on.
	 * @param adress The resolved ipadress.
	 */
	public static void startMessage(String adress)
	{
		System.out.println(getFullTimeStamp() + "Starting server instance on '" + adress + "'." );
	}
	/**
	 * Writes to the "chat_log_date.txt"
	 * @param sender Senders name
	 * @param msg Message to add to chatlog
	 */
	public static void writeToChatLog(String sender, String msg) {
		try{
			File log = new File("chatlogs", "chat_log_" +getDateStamp()+ ".txt");
			if(new File("chatlogs").mkdirs())
				Output.log("OUTPUT", "Created chatlogs directory.");
			BufferedWriter chatlogger = new BufferedWriter(new FileWriter(log, true));
			chatlogger.write("[" +Output.getTimeStamp()+ "] [" +sender+ "] " +msg);;
			chatlogger.newLine();
			chatlogger.close();
		}
		catch(Exception e)
		{
			Output.log("OUTPUT", "Error when writing chatlog to file!");
		}
	}
	/**
	 * Creates a datestamp in the format 'Year-Month-Day'.
	 * @return The datestamp
	 */
	private static String getDateStamp() {
		Calendar calendar = new GregorianCalendar();
		String day = ""+calendar.get(Calendar.DAY_OF_MONTH);
		String month = ""+calendar.get(Calendar.MONTH);
		String year = ""+calendar.get(Calendar.YEAR);
		
		if(day.length() == 1)
			day = "0"+day;
		if(month.length() == 1)
			month = "0"+month;
		if(year.length() == 1)
			year = "0"+year;
		
		return year + "-" + month + "-" + day;	
	}
}
