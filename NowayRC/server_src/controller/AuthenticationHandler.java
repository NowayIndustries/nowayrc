package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import view.Output;
import model.ServerComponent;

public class AuthenticationHandler  {

	String UUID;
	
	public AuthenticationHandler()
	{
		UUID = "NO_VALID_AUTH_RETURNED";
	}
	
	public String fetchUUID(String authserverurl, String username, String passwordhash)
	{
		try{
			URL ipresolver = new URL(authserverurl+"?user="+username+"&pass="+passwordhash);
			BufferedReader in = new BufferedReader(new InputStreamReader(ipresolver.openStream()));
		
			String response = in.readLine();
			
			if(response.substring(0, 1).compareToIgnoreCase("UU") == 0)
			{
				UUID = response;
			}
		}
		catch(MalformedURLException MURLE)
		{
			Output.log("AUTH","Unable to resolve the authentication url, please check the url provided in the config.");
		}
		catch(IOException IOE)
		{
			Output.log("AUTH", "Unable to connect to the authentication server, check your internet connection.");
		}
		return UUID;
	}
	
	public String getUUID()
	{
		return UUID;
	}

}
