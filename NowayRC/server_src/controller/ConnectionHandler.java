package controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

import view.Output;
import model.Client;
import model.EncryptedInputStream;
import model.EncryptedOutputStream;
import model.ServerComponent;

public class ConnectionHandler implements ServerComponent, Runnable{
	private ArrayList<String> blacklist = new ArrayList<String>();
	private boolean keepRunning = false;
	
	private ServerSocket serverSocket;
	private int portNumber;
	private String host;
	private int maximumConnections;
	private String motd;
	private String[] presets = { "Server", "IP", "INFO", "MOTD", "LIST", "UUID_BANNED" };
	private String secret = "secret";
	private AuthenticationHandler auth;
	
	/**
	 * Creates a new connection handler, using the values set in the config.
	 */
	public ConnectionHandler()
	{
		keepRunning = false;
		portNumber = config.getPort();
		host = config.getHost();
		maximumConnections = config.getMaxConnections();
		motd = config.getMotd();
		for(String s : presets)
		{
			blacklist.add(s);
		}
		auth = new AuthenticationHandler();
	}
	
	/**
	 * Reads the blacklist.txt and adds the names to the list.
	 */
	public void readBlacklist()
	{
		BufferedReader blacklistFile;
		try
		{
			blacklistFile = new BufferedReader(new FileReader(config.getBlackListFile()));
			String name = blacklistFile.readLine();
			while(blacklistFile.ready() && name != null)
			{
				addToBlacklist(name);
				name = blacklistFile.readLine();
			}
			blacklistFile.close();
		}
		catch(Exception e)
		{
			
		}
		printBlacklist();
	}
	
	/**
	 * Checks if a client is blacklisted (pulls name from the clientobject and then calls isBlacklisted(string).
	 * @param c The client to check
	 * @return True if client's name is in the blacklist, else false.
	 */
	private boolean isBlacklisted(Client c)
	{
		return blacklist.contains(c.getName());
	}
	/**
	 * Checks to see if a string is in the blacklist.
	 * @param u Name to check for.
	 * @return True if the name is in the blacklist, else false.
	 */
	private boolean isBlacklisted(String u)
	{
		for(String s : blacklist)
		{
			if(s.compareToIgnoreCase(u) == 0)
			{
				return true;
			}
		}
		return false;
	}
	/**
	 * Adds the specified name to the blacklist (blacklist is only saved onShutdown() is called!).
	 * @param user Name to add.
	 * @return True if the user was added, false if user was already banned.
	 */
	public boolean addToBlacklist(String user)
	{
		if(!isBlacklisted(user))
		{	
			blacklist.add(user);
			return true;
		}
		else{
			Output.log("CONHAN", "User '" +user+ "' is already banned.");
			return false;
		}
	}
	/**
	 * Prints the blacklist.
	 */
	public void printBlacklist()
	{
		String msg = "Users in blacklist: ";
		
		for(String s : blacklist)
		{
			msg += s+", ";
		}
		Output.log("CONHAN", msg);
	}
	
	//***************************************************************************************************************\\
	private Socket s;
	public void run() 
	{
		onStart();
		EncryptedInputStream in = null;
		EncryptedOutputStream out = null;
		while(keepRunning)
		{
			try 
			{
				s = serverSocket.accept();
				in = new EncryptedInputStream(new ObjectInputStream(s.getInputStream()), secret);
				out = new EncryptedOutputStream(new ObjectOutputStream(s.getOutputStream()), secret);
				String handshake = (String)in.receive();
				
				String[] info = handshake.split(";", 4);
				String protocol = info[0];
				String sharedsecret = info[1];
				String n = info[2];
				String p = info[3];
				
				//example handshake '1;securepassword;Smith;passwordHash'
				//info[0] = protocol version
				//info[1] = sharedsecret (to be used for encryption)
				//info[2] = name given.
				//info[3] = the password hashed (password use to authenticate against authserver 
				//OR against 'serverpass' in config if it is set, taking priority over the authserver)
				
				//Protocol check
				boolean unsupported = false;
				for(String v : config.getSupportedProtocolVersions())
				{
					if (v.compareToIgnoreCase(protocol) > 0)
					{
						unsupported = true;
					}
				}
				if(unsupported)
				{
					disconnect(s, out, "This server does not support your client.");
					continue;
				}
				//Protocol check end
				if(config.getServerPassword().compareToIgnoreCase("") == 0)
				{
					if(config.getAuthServerUrl().compareToIgnoreCase("") != 0)
					{
						auth.fetchUUID(config.getAuthServerUrl(), n, p);
						if(isBlacklisted(auth.getUUID()))
						{
							Output.log("CONHAN", "A user which is not registered on the authserver has tried to connect.");
							disconnect(s, out, "Invalid authentication credentials entered!");
							continue;
						}
					}
				}else if(config.getServerPassword().compareToIgnoreCase(p) != 0){
					Output.log("CONHAN", "A user has entered an incorrect serverpassword.");
					disconnect(s, out, "Invalid credentials entered!");
					continue;
				}
				
				//purging passwordhash
				info[3] = "";
				p = "";
				
				if(isBlacklisted(n) || isBlacklisted(auth.getUUID()) || isBlacklisted(s.getInetAddress().getHostAddress()))
				{
					Output.log("CONHAN", "A banned user has tried to connect.");
					disconnect(s, out, "You are banned on this server!");
					continue;
				}
				
				if(clientHandler.findClient(n) != null)
				{
					disconnect(s, out, "That name is taken.");
					continue;
				}
				
				Output.log("CONHAN", "A new client with the ip '" +s.getInetAddress().getHostAddress()+ "' has connected.");
				out.send("", motd);
				clientHandler.newClient( s, in, out, n, this.secret, sharedsecret, protocol);
				
				Thread.sleep(100);
			} 
			catch (SocketException e)
			{
				//lulz we be exiting the program.
			}
			catch (InterruptedException e) 
			{
				
			} catch (IOException e) {
				e.printStackTrace();
				Output.log("CONHAN", "Error when accepting connection from connecting client!");
				e.printStackTrace();
			}
		}
	}
	
	public void onStart() 
	{
		Output.log("CONHAN", "Reading blacklist file");
		readBlacklist();
		keepRunning = true;
		
		//SocketShit :)
		Output.log("Preparing serversocket on '" +host+ "' at port: " +portNumber+ ". With a maximum of " +maximumConnections+ " connections.");
		try{
			InetAddress inetAdress = InetAddress.getByName(host);
			serverSocket = new ServerSocket(portNumber, maximumConnections);
		}
		catch(Exception e)
		{
			Output.log("Unable to start server, binding serversocket failed! Did you configure the server corretly?");
			Output.log(e.getMessage());
			main.main.exit();
		}
	}
	public void onShutdown()
	{
		Output.log("CONHAN", "Saving blacklist to file '" +config.getBlackListFile()+ "'");
		for(String s : presets)
		{
			blacklist.remove(s);
		}
		
		try{
			BufferedWriter blacklistWriter = new BufferedWriter(new FileWriter(config.getBlackListFile()));
			for(String s : blacklist)
			{
				blacklistWriter.write(s);
				blacklistWriter.newLine();
			}
			blacklistWriter.write(" ");
			blacklistWriter.newLine();
			blacklistWriter.close();
		}
		catch(Exception e)
		{
			Output.log("CONHAN", "Error when writing blacklist to file!");
		}
		try
		{
			serverSocket.close();
		}
		catch(IOException ioe)
		{
			Output.log("Unable to close serversocket!");
		}
		Output.log("CONHAN", "Saved " +blacklist.size()+ " names in blacklist. (" +presets.length+ " names are software-banned)");
	}
	
	public void shutdown()
	{
		keepRunning = false;
		onShutdown();
		System.exit(0);
	}
	
	public void updateHost(String h)
	{
		this.host = h;
	}
	public void updatePort(int p)
	{
		this.portNumber = p;
	}
	/**
	 * Updates the socket information, does not renew it.
	 * @param h Host string.
	 * @param p Portnumber
	 */
	public void updateServerSocketInfo(String h, int p)
	{
		updateHost(h);
		updatePort(p);
	}

	public void removeFromBlacklist(String user) {
		blacklist.remove(user);		
	}
	public String getConnectionLimit()
	{
		return ""+this.maximumConnections;
	}
	/**
	 * Disconnects the socket, but sends them the reason first.
	 * @param s Socket to close.
	 * @param out ObjectOutputStream for the socket.
	 * @param reason Reason that has been supplied.
	 */
	private void disconnect(Socket s, EncryptedOutputStream out, String reason)
	{
		try {
			out.send("", reason);
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Disconnects the client, but first sends them the reason.
	 * @param c Client to disconnect.
	 * @param reason Reason that has been supplied.
	 */
	public void disconnect(Client c, String reason)
	{
		disconnect(c.getSocket(),c.getEncryptedOutputStream(), reason);
	}
}
