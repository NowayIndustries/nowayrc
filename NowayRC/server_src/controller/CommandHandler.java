package controller;

import javax.swing.JOptionPane;

import model.Commands;
import model.ServerComponent;
import view.Output;

public class CommandHandler implements ServerComponent, Runnable{
	
	private boolean keepRunning = false;
	/**
	 * Used to identify commands send through the server application, the correct methods are then called.
	 * @param input String the user entered in the server application.
	 */
	public void sendInput(String input)
	{
		String[] processedString = extractCommand(input);

		if(processedString.length < 1)
		{
			return;
		}
		
		String command = processedString[0];
		String parameter = "";
		if(processedString.length == 2)
		{
			parameter = processedString[1];
		}
		
		int commandID = -1;
		boolean matched = false;
		
		for(int i = 0; i < Commands.commandList.length && !matched; i++)
		{
			//System.out.println("Length: "+Commands.commandList.length +" Index: "+i +"LengthCheck: "+ (i < Commands.commandList.length) +" !matched: "+!matched +" Command: '"+Commands.commandList[i]+"'");
			if(command.equalsIgnoreCase(Commands.commandList[i]))
			{
				commandID = i;
				matched = true;
			}
		}
		
		switch(commandID)
		{
		case 0:
			Commands.say(parameter);
			break;
		case 1:
			Commands.kick(parameter);
			break;
		case 2:
			Commands.ban(parameter);
			break;
		case 3:
			Commands.stop();
			break;
		case 4:
			Commands.unban(parameter);
			break;
		case 5:
			Commands.help();
			break;
		case 6:
			Commands.list();
			break;
		case 7:
			Commands.check();
			break;
		case 8:
			Commands.version();
			break;
		case 9:
			Commands.reset(parameter);
			break;
		default:
			Output.log("Unrecognized command! Try again.");
		}
		
	}
	/**
	 * Used to identify commands send by client applications.
	 * @param in String sent by client which starts with '@'.
	 */
	public String[] textCommand(String in) {
		String[] processedString = in.split(" ", 1);
		
		String command = processedString[0];
		command = command.substring(1);
		
		int commandID = -1;
		boolean matched = false;
		
		for(int i = 0; i < Commands.textCommandList.length && !matched; i++)
		{
			//System.out.println("Length: "+Commands.commandList.length +" Index: "+i +"LengthCheck: "+ (i < Commands.commandList.length) +" !matched: "+!matched +" Command: '"+Commands.commandList[i]+"'");
			if(command.equalsIgnoreCase(Commands.textCommandList[i]))
			{
				commandID = i;
				matched = true;
			}
		}
		
		switch(commandID)
		{
		case 0:
			return Commands.textIp();
		case 1:
            return Commands.textList();
		case 2:
            return Commands.textMotd();
		case 3:
            return Commands.textHelp();
		case 4:
            return Commands.textVersion();
		default:
		}
        String[] d = {"COMMAND", "Invalid command."};
        return d;
	}
	
	/**
	 * Splits the string on ' '.
	 * @param input Raw input from server application
	 * @return Array, containing the command [0] and the parameter [1].
	 */
	private String[] extractCommand(String input)
	{
		return input.split(" ", 2);
	}
	
	//***************************************************************************************************************\\
	
	public void run() 
	{
		onStart();
		
		while(keepRunning)
		{
			try 
			{
				Thread.sleep(100);
			} 
			catch (InterruptedException e) 
			{
				
			}
		}
		
		onShutdown();
	}
	
	public void onStart() 
	{
		keepRunning = true;
	}
	public void onShutdown() 
	{
		
	}	
	
	public void shutdown()
	{
		keepRunning = false;
	}
}
