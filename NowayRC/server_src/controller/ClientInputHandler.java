package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.Socket;

import javax.swing.JOptionPane;

import model.Client;
import model.Commands;
import model.Protocol;
import model.ServerComponent;
import view.Output;

public class ClientInputHandler implements Runnable, ServerComponent
{
	private Client client;
	private boolean keepRunning;
	private ClientHandler handler;
	
	/**
	 * Creates a new clientinputhandler, also broadcasts the connecion of a new client.
	 * @param c The client it is supposed to handle.
	 * @param ch The clienthandler for that client.
	 */
	public ClientInputHandler(Client c, ClientHandler ch)
	{
		try {
			client = c;
			handler = ch;

			Commands.say("A new client with the name '" +client.getName()+ "' has connected.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		onStart();
		String in = "test";
		while(keepRunning)
		{
			try {
				in = (String)client.getEncryptedInputStream().receive();
				
				if(in == null)
				{
					Output.log("CLIHAN", "Client's recv has returned null, assuming broken socket.");
					this.clientHandler.disconnect(getClient().getName(), "Assumed broken socket");
					this.shutdown();
				}
				else if(in.compareToIgnoreCase(Protocol.getDisconnectCode()) == 0){
					clientHandler.disconnect(getClient().getName(), "has quit");
                    handler.sendAllRaw("[PLAYERLISTING]"+clientHandler.getList());
				}else if(in.substring(0, 1).compareTo(Protocol.getCommandCode()) == 0){
                    String[] r = commandHandler.textCommand(in);
					clientHandler.send(r[0], r[1], client);
                }else if(in.compareTo(Protocol.getListCode()) == 0){
                    handler.sendAllRaw("[PLAYERLISTING]"+clientHandler.getList());
                }else
					handler.sendAll(client.getName(), in);
				
				
		
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		onShutdown();
	}
	
	/**
	 * Returns the client that this clientinputhandler responsible for is.
	 * @return The client goverend by this clientinputhandler.
	 */
	public Client getClient()
	{
		return client;
	}

	@Override
	public void onStart() {
		keepRunning = true;
	}

	@Override
	public void onShutdown() {
		try {
			client.getSocket().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void shutdown() {
		keepRunning = false;
	}
}
