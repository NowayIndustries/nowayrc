package controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import view.Output;
import model.Client;
import model.Commands;
import model.EncryptedInputStream;
import model.EncryptedOutputStream;
import model.Protocol;
import model.ServerComponent;

public class ClientHandler implements ServerComponent, Runnable{
	
	private boolean keepRunning = false;
	private ArrayList<Client> connectedClients = new ArrayList<Client>();
	private ArrayList<ClientInputHandler> clientInputHandlers = new ArrayList<ClientInputHandler>();
	
	/**
	 * Creates string with the format '[TIME][SENDER] msg' and sends it to all connected clients.
	 * @param sender Sender of the message.
	 * @param msg Message to be sent.
	 */
	public void sendAll(String sender, String msg)
	{
		Output.log("CLIHAN", "Received input from '" +sender+"', sending to all: '" +msg+ "'");
		Output.writeToChatLog(sender, msg);
		String prefix = Output.getTimeStamp()+ " [" +sender+ "] ";
		//Go through list of clients, send them all the compounded message.
		for(Client c : connectedClients)
		{
			c.send(prefix, msg);
		}
	}

    /**
     * Sends msg to specifick client.
     * @param sender Source
     * @param msg Message
     * @param c Target
     */
    public void send(String sender, String msg, Client c)
    {
        Output.log("CLIHAN", "Received input from '" +sender+"', sending to '"+c.getName()+"': '"+msg+"'");
        Output.writeToChatLog(sender, msg);
        String prefix = Output.getTimeStamp()+ " [" +sender+ "] ";
        c.send(prefix, msg);
    }

    /**
     * Sends param s to all clients, without adding or modifing s.
     * @param s String to be sent.
     */
    public void sendAllRaw(String s) {
        Output.log("CLIHAN", "Sending new connectionlist to all clients.");
        Output.writeToChatLog("PLAYERLIST",s);
        //Go through list of clients, send them all the compounded message.
        for(Client c : connectedClients)
        {
            c.getEncryptedOutputStream().sendRaw(s);
        }
    }
	/**
	 * Tries to find the client object with the given name in connectedClients.
	 * @param user Name of user who's clientobject we want.
	 * @return Returns the client if found, else null.
	 */
	public Client findClient(String user){
		Client subject = null;
		for(Client c : connectedClients)
		{
			if(c.getName().compareToIgnoreCase(user) == 0)
			{
				subject = c;
				break;
			}
		}
		return subject;
	}
	/**
	 * Disconnects the user by stopping the clientinputhandler for that clientobject and calling disconnect in connectionHandler. (uses findClient())
	 * @param user Name of client to disconnect.
	 * @param reason The reason of disconneciton.
	 */
	@SuppressWarnings("static-access")
	public void disconnect(String user, String reason)
	{
		//Find user with name (user) and disconnect them with reason as message.
		Client subject = findClient(user);
		if(subject != null)
		{
			connectionHandler.disconnect(subject, reason);
			for(ClientInputHandler cih : clientInputHandlers)
			{
				if(cih.getClient().equals(subject))
				{
					cih.shutdown();
					break;
				}
			}
			connectedClients.remove(subject);
			sendAll("Server", "'" +user+ "' " +reason+ ".");
		}else{
			Output.log("CLIHAN", "Unable to find '" +user+ "' to disconnect. Reason:'" +reason+ "'.");
		}
	}
	/**
	 * Forms the list of people connected to the server (by name).
	 * @return The string containing all the names of the connected clients.
	 */
	public String getList() {
		if(!connectedClients.isEmpty())
		{
			String list = "(" +connectedClients.size()+ "/" +connectionHandler.getConnectionLimit()+ ") [ ";
			for(Client c : connectedClients)
			{
				list += c.getName() +" | ";
			}
			list = list.substring(0, list.lastIndexOf("|") - 1);
			list += " ]";
			return list;
		}
		return "[ No users connected ]";
	}
	
	//***************************************************************************************************************\\
	
		public void run() 
		{
			//No need to keep a thread running for this handler, however ServerComponent requires it.
		}
		
		public void onStart() 
		{
			keepRunning = true;
		}
		public void onShutdown() 
		{
			for(ClientInputHandler cih : clientInputHandlers)
			{
				cih.shutdown();
			}
		}
		
		
		public void shutdown()
		{
			onShutdown();
		}
		/**
		 * Creates a new client and adds it to the lists. Then creates an input handler for the client.
		 * @param s The socket for the client.
		 * @param in The objectinputstream for the client.
		 * @param out The objecoutputstream for the client.
		 * @param n The name for the client.
		 * @param sharedsecret password used to obfuscate the communication.
		 * @param protocol version number which the client whishes to use.
		 */
		public void newClient(Socket s, EncryptedInputStream in, EncryptedOutputStream out, String n, String secret, String sharedsecret, String protocol) 
		{			
			Client c = new Client(s, in, out, n, secret, sharedsecret, protocol);
			connectedClients.add( c );
			ClientInputHandler cih = new ClientInputHandler(c, this);
			clientInputHandlers.add( cih );
			new Thread(cih).start();
			Output.log("CLTHAN", "A new user has accepted, there are now '" +(connectedClients.size())+ "' connected clients.");
		}
		/**
		 * Checks the connectedClients list by trying to write an string ("0x000001") through the socket. The client ignores strings starting with 0x.
		 * @return Returns a string, listing the amount of alive and dead connections, or "no clients connected".
		 */
		public String check() {
			if(!connectedClients.isEmpty())
			{
				String response = "(" +connectedClients.size()+ "/" +connectionHandler.getConnectionLimit()+ ") [ ";
				int alive = 0;
				int dead = connectedClients.size();
				ArrayList<Client> disconnect = new ArrayList<Client>();
				for(Client c : connectedClients)
				{
					c.getEncryptedOutputStream().send("",Protocol.getCheckCode());
					alive++;
					dead--;
				}
				for(Client c : disconnect)
				{
					c.disconnect();
					connectedClients.remove(c);
					Commands.say("Client '" +c.getName()+ "' has been disconnected by the clientcheck.");
				}

				response = "(" +alive+ "/" +connectionHandler.getConnectionLimit()+ ") alive, (" 
							+dead+ "/" +connectionHandler.getConnectionLimit()+ ") dead. Dead clients were disconnected.";
				return response;
			}
			return "[ No users connected ]";
		}
}
