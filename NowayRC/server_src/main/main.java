package main;
import java.io.Console;
import java.util.Scanner;

import model.Config;
import model.IPAdress;
import model.ServerComponent;
import view.Output;


public abstract class main implements ServerComponent {
	
	private static boolean keepRunning = false;
	
	public static void main(String[] args) 
	{
		init();
		run();
	}
	
	private static void init()
	{
		Output.startMessage(IPAdress.getExternalIP());
		
		Output.log("Sending start message to ClientHandler");
		clientHandlerThread.start();
		
		Output.log("Sending start message to ConnectionHandler");
		connectionHandlerThread.start();
		
		Output.log("Sending start message to CommandHandler");
		commandHandlerThread.start();
		
		keepRunning = true;
	}
	
	public static void exit()
	{
		Output.log("Shutting down server.");
		
		Output.log("Sending shutdown message to ClientHandler");
		clientHandler.shutdown();
		
		Output.log("Sending shutdown message to ConnectionHandler");
		connectionHandler.shutdown();
		
		Output.log("Sending shutdown message to CommandHandler");
		commandHandler.shutdown();
		
		keepRunning = false;
		System.exit(0);
		
	}
	
	private static void run()
	{
		Scanner console = new Scanner(System.in);
		String input = "";
		Output.log("Now accepting input:");
		while(keepRunning)
		{
			input = console.nextLine();
			commandHandler.sendInput(input);
		}
		console.close();
	}
}
